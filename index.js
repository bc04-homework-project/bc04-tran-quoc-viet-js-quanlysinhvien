const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

var dssv = [];

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);

  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];

    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }

  renderDSSV(dssv);
}

function themSV() {
  var newSv = layThongTinTuForm();
  // console.log("newSv: ", newSv);

  var isValid1 = isValid(newSv);
  if (isValid1) {
    dssv.push(newSv);

    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
  }

  renderDSSV(dssv);
}

function xoaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  // console.log("index: ", index);
  if (index != -1) {
    dssv.splice(index, 1);

    localStorage.setItem(DSSV_LOCALSTORAGE, JSON.stringify(dssv));

    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  // console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
  document.getElementById("txtMaSV").disabled = true;
}

var capNhatSV = () => {
  var fixSV = layThongTinTuForm();
  var isValid1 = isValid(fixSV);
  var index = timKiemViTri(fixSV.ma, dssv);
  if (isValid1) {
    dssv[index] = fixSV;

    localStorage.setItem(DSSV_LOCALSTORAGE, JSON.stringify(dssv));
  }

  renderDSSV(dssv);
  resetForm();
  document.getElementById("txtMaSV").disabled = false;
};
