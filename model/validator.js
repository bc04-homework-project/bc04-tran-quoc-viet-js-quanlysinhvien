var validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraDoDai: function (value, idError, message, min, max) {
    // console.log(value);
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraEmail: function (value, idError, message) {
    const re =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (re.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
};

function isValid(sv) {
  var isValid =
    validator.kiemTraRong(sv.ma, "spanMaSV", "Mã sinh viên không được rỗng") &&
    validator.kiemTraDoDai(
      sv.ma,
      "spanMaSV",
      "Mã sinh viên phải có 4 ký tự",
      4,
      4
    );

  isValid =
    isValid &
      validator.kiemTraRong(sv.email, "spanEmailSV", "Email không được rỗng") &&
    validator.kiemTraEmail(sv.email, "spanEmailSV", "Email không hợp lệ");

  isValid =
    isValid &
    validator.kiemTraRong(
      sv.ten,
      "spanTenSV",
      "Tên sinh viên không được rỗng"
    ) &
    validator.kiemTraRong(
      sv.matKhau,
      "spanMatKhau",
      "Mật khẩu không được rỗng"
    ) &
    validator.kiemTraRong(sv.toan, "spanToan", "Điểm toán không được rỗng") &
    validator.kiemTraRong(sv.ly, "spanLy", "Điểm lý không được rỗng") &
    validator.kiemTraRong(sv.hoa, "spanHoa", "Điểm hóa không được rỗng");

  return isValid;
}
